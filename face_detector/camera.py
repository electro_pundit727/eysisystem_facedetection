import threading
import time
import cv2
import datetime
import numpy
import os
from picamera import PiCamera
from picamera.array import PiRGBArray

from settings import CAMERA_RESOLUTION, SHOW_VIDEO, logger, FACE_MIN_SIZE
from src.face.face_detector import FaceDetector
from src.utils.barcode_util import get_ap_info_from_barcode
from src.utils.common import get_mac_address

capture_path = '/tmp/capture.jpg'

SIMILARITY_THRESHOLD = 60
FACE_TIMEOUT = 2


class CameraService(threading.Thread):

    b_stop = threading.Event()
    cur_faces = []
    queue = None
    path = '/tmp'

    def __init__(self, queue, path='/tmp'):
        super(CameraService, self).__init__()
        self.cam = PiCamera(resolution=CAMERA_RESOLUTION)
        self.detector = FaceDetector(min_size=FACE_MIN_SIZE)
        self.b_stop.clear()
        self.queue = queue
        if not os.path.exists(path):
            os.makedirs(path)
        self.path = path

    def scan_wifi_qr_code(self):
        self.cam.capture(capture_path)
        return get_ap_info_from_barcode(capture_path)

    def run(self):
        raw_capture = PiRGBArray(self.cam, self.cam.resolution)
        time.sleep(2)

        for img in self.cam.capture_continuous(raw_capture, format='bgr', use_video_port=True):
            s_time = time.time()
            frame = numpy.asarray(img.array)
            faces = self.detector.detect_face(frame)
            if SHOW_VIDEO:
                show_video_frame(frame, faces)
            if faces:
                print datetime.datetime.now(),  ':  Detected faces: ', [f['rect'] for f in faces]
                for cur_face in self.cur_faces:
                    rect_list = [f['rect'] for f in faces]
                    f = find_similar_face(cur_face['rect'], rect_list)
                    if f is not None:
                        face_data = faces[rect_list.index(f)]
                        cur_face.update(face_data)
                        cur_face['last_seen'] = time.time()
                        faces.remove(face_data)

            for cur_face in self.cur_faces[:]:
                if time.time() - cur_face['last_seen'] > FACE_TIMEOUT:
                    self.upload_disappeared_face(cur_face)
                    self.cur_faces.remove(cur_face)

            # Add new found faces
            for f in faces:
                f.update({'last_seen': time.time()})
                self.cur_faces.append(f)

            elapsed = time.time() - s_time
            print datetime.datetime.now(),  ':  Current Faces: {}, Elapsed: {}'.format(len(self.cur_faces), elapsed)
            if self.b_stop.isSet():
                break
            if SHOW_VIDEO and cv2.waitKey(1) & 0xFF == ord('q'):
                break
            raw_capture.truncate(0)

        self.cam.close()

    def upload_disappeared_face(self, face):
        file_name = os.path.join(
            self.path, 'face_{}_{}.jpg'.format(get_mac_address().replace('-', ''), int(face['last_seen'])))
        logger.info('A face({}) is disappeared, saving to a file - {}'.format(face['rect'], file_name))
        cv2.imwrite(file_name, face['frame'])
        self.queue.put(file_name)

    def stop(self):
        self.b_stop.set()


def find_similar_face(face_rect, rect_list):
    for r in rect_list:
        if get_overlap_percentage(face_rect, r) > SIMILARITY_THRESHOLD:
            return r


def get_overlap_percentage(rect1, rect2):
    """
    Compute intersect area of 2 rectangles
    :param rect1: [x, y, w, h]
    :param rect2: [x, y, w, h]
    :return:
    """
    w = max(0, min(rect1[0] + rect1[2], rect2[0] + rect2[2]) - max(rect1[0], rect2[0]))
    h = max(0, min(rect1[1] + rect1[3], rect2[1] + rect2[3]) - max(rect1[1], rect2[1]))
    ratio = w * h / float(rect1[2] * rect1[3]) * 100
    print '=====', rect1, " <=> ", rect2
    print 'Ratio: ', ratio, '\n'
    return ratio


def show_video_frame(frame, faces):
    for (x, y, w, h) in [f['rect'] for f in faces]:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0, 2))
    try:
        cv2.imshow('Faces', frame)
    except:
        pass


if __name__ == '__main__':
    from Queue import Queue
    import shutil
    _queue = Queue()
    inst = CameraService(queue=_queue)
    inst.start()
    while True:
        if _queue.qsize() > 0:
            img_file = _queue.get()
            shutil.copy(img_file, os.path.expanduser('~/Pictures'))
