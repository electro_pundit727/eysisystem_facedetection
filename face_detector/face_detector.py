import os
import cv2


class FaceDetector:

    def __init__(self, min_neighbors=5, min_size=(200, 200)):
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'haarcascade_frontalface_alt.xml')
        self.cascade = cv2.CascadeClassifier(path)
        self.min_neighbors = min_neighbors
        self.min_size = min_size

    def detect_face(self, img, resize=False):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        result_data = []
        if resize:
            gray = cv2.resize(gray, (512, 512))  # resize image to 512x512
        results = self.cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=self.min_neighbors,
                                                minSize=self.min_size, flags=cv2.CASCADE_SCALE_IMAGE)
        if results != ():
            for r in results:
                # Extend rectangle to get hair, ear, etc
                height, width = img.shape[:2]
                r[0] = max(r[0] - r[2] * .1, 0)
                r[1] = max(r[1] - r[3] * .2, 0)
                r[2] = min(r[2] + r[2] * .2, width)
                r[3] = min(r[3] + r[3] * .4, height)
                # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
                result_data.append({
                    'rect': r,
                    'frame': img[r[1]:r[1] + r[3], r[0]:r[0] + r[2]],
                })

        # Sort frames with their sizes
        sorted_data = sorted(result_data, key=lambda f: f['rect'][2] * f['rect'][3], reverse=True)
        return sorted_data


if __name__ == '__main__':

    cap = cv2.VideoCapture(0)
    det = FaceDetector()

    while True:
        ret, sample_img = cap.read()
        if ret:
            sample_img = cv2.flip(sample_img, 1)
            rects = det.detect_face(sample_img, resize=False)
            for rect in rects:
                (x, y, w, h) = rect['rect']
                cv2.rectangle(sample_img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.imshow('img', sample_img)
            key = cv2.waitKey(1) & 0xFF

            # if the `q` key was pressed, break from the loop
            if key == ord("q"):
                break
    cv2.destroyAllWindows()
